<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create Book</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>

<div class="container">

<div class="col-md-6 col-md-offset-3">

<form action="store.php" method="POST">
    <div class="form-group">
        <label for="title">Book Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Title">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>

</div>

</div>

</body>
</html>
