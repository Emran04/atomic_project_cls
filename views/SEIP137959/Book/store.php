<?php
include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\Book\Book;


$myBook = new Book();

$myBook->prepare($_POST);
$myBook->store();