<?php

session_start();

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\Book\Book;
use App\Bitm\SEIP137959\Book\Message;
//use App\Bitm\SEIP137959\Book\Utility;

$myBook = new Book();

$books = $myBook->index();

//Utility::d($books);

if(array_key_exists('message', $_SESSION)) {
    $message = Message::message();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create Book</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>

<div class="container">

    <div class="col-md-8 col-md-offset-2">

        <h2>Books</h2>

        <p>
            <a href="create.php" class="btn btn-primary">Create New Book</a>
        </p>
        <div>
            <?php if(!empty($message))echo $message; ?>
        </div>
        <table class="table table-striped">
            <tr class="active">
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>

            <?php
            $sl = 0;
            foreach($books as $book) :
                $sl++;  ?>
                <tr>
                    <td><?php echo $sl; ?></td>
                    <td><?php echo $book['id']; ?></td>
                    <td><?php echo $book['title']; ?></td>
                    <td>
                        <a href="view.php" class="btn btn-primary">View</a>
                        <a href="edit.php" class="btn btn-primary">Edit</a>
                        <a href="delete.php" class="btn btn-danger">Delete</a>
                        <a href="trash.php" class="btn btn-danger">Trash</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>

</div>

</body>
</html>
