<?php

session_start();

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP137959\Email\Email;
use App\Bitm\SEIP137959\Book\Message;
//use App\Bitm\SEIP137959\Book\Utility;

$emails = new Email();

$allEmails = $emails->index();

//Utility::d($allEmails);
if(array_key_exists('message', $_SESSION)) {
    $message = Message::message();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email lists</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>

<div class="container">

    <div class="col-md-8 col-md-offset-2">

        <h2>Emails</h2>

        <p>
            <a href="create.php" class="btn btn-primary">Create New Email</a>
        </p>
        <?php if(!empty($message)) : ?>
            <div id="message" class="alert alert-success" role="alert">
                <?php echo $message;  ?>
            </div>
        <?php endif; ?>
        <table class="table table-striped">
            <tr class="active">
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Action</th>
            </tr>

            <?php
            $sl = 0;
            foreach($allEmails as $email) :
                $sl++;  ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td><?php echo $email['id']; ?></td>
                <td><?php echo $email['email']; ?></td>
                <td>
                    <a href="view.php?id=<?php echo $email['id']; ?>" class="btn btn-primary">View</a>
                    <a href="edit.php" class="btn btn-primary">Edit</a>
                    <a href="delete.php" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>

    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>
    $('#message').delay(3000).fadeOut();
</script>

</body>
</html>
