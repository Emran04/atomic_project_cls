<?php

namespace App\Bitm\SEIP137959\Book;
class Utility
{
    public static function d($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
    }

    public static function redirect($data)
    {
        header('Location:' . $data);
    }
}