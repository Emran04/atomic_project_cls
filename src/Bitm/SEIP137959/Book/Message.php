<?php
namespace App\Bitm\SEIP137959\Book;

if(!isset($_SESSION['message'])) {
    session_start();
}

class Message
{
    public static function message($message = null)
    {
        if(is_null($message)) {
            return self::getMessage();
        } else {
            self::setMessage($message);
        }
    }

    public static function setMessage($message)
    {
        $_SESSION['message'] = $message;
    }

    public static function getMessage()
    {
        $message = $_SESSION['message'];
        $_SESSION['message'] = '';

        return $message;
    }
}