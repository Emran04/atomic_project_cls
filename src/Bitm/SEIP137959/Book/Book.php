<?php
namespace App\Bitm\SEIP137959\Book;

class Book {
    public $id,
        $title,
        $created,
        $modified,
        $created_by,
        $modified_by,
        $deleted_at,
        $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojectb22') or die("Database connection failed");
    }

    public function index(){
        $_allBook = array();
        $query="SELECT * FROM `atomicprojectb22`.`books`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allBook[]=$row;
        }

        return $_allBook;
    }

    public function create()
    {
        return 'I am creating form';
    }

    public function prepare($data = "") {
        if(array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }

        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

    }

    public function store(){
        $query = "INSERT INTO `atomicprojectb22`.`books` (`title`) VALUES ('".$this->title."')";

        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::setMessage("Data Has been stored successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::setMessage("Error");
        }
    }

    public function edit()
    {
        return 'I am editing data';
    }

    public function update()
    {
        return 'I am updating data';
    }
    public function delete()
    {
        return 'I am deleting data';
    }

    public function view()
    {
        return 'view data';
    }
}