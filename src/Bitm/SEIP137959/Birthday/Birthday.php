<?php
class Birthday {
    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct()
    {

    }

    public function index()
    {
        //I am listing data
    }

    public function create()
    {
        // I am creating form
    }

    public function store()
    {

    }

    public function edit()
    {

    }

    public function update()
    {

    }
    public function delete()
    {

    }

    public function view()
    {

    }
}