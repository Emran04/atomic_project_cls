<?php
namespace App\Bitm\SEIP137959\Email;

use App\Bitm\SEIP137959\Book\Message;
use App\Bitm\SEIP137959\Book\Utility;

class Email {
    public $id;
    public $email;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;
    public $conn;

    public function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', '', 'atomicprojectb22') or die("Database connection failed");
    }

    public function prepare($data = array())
    {
        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
    }

    public function index(){
        $_allEmails = array();
        $query="SELECT * FROM `atomicprojectb22`.`emails`";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allEmails[]=$row;
        }

        return $_allEmails;
    }

    public function view()
    {
        $query = "SELECT * FROM `atomicprojectb22`.`emails` WHERE `id` =" .$this->id;
        $result= mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function create()
    {
        return 'I am creating form';
    }

    public function store(){
        $query = "INSERT INTO `atomicprojectb22`.`emails` (`email`) VALUES ('".$this->email."')";

        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::setMessage("Data Has been stored successfully");
            Utility::redirect('index.php');
        }
        else {
            Message::setMessage("Error");
        }
    }

    public function edit()
    {
        return 'I am editing data';
    }

    public function update()
    {
        return 'I am updating data';
    }
    public function delete()
    {
        return 'I am deleting data';
    }
}